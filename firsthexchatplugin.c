#include "hexchat-plugin.h"

/* plugin info  used in hexchat_plugin_get_info*/
#define PNAME "AutoOp"
#define PDESC "Auto Ops anyone that joins"
#define PVERSION "0.1"

static hexchat_plugin *ph; /* plugin handle */
static int enable = 1;

static int join_cb(char *word[], void *userdata) {
	if (enable)
		hexchat_commandf(ph, "OP %s", word[1]);
	/* word[1] is the nickname */
	
	return HEXCHAT_EAT_NONE; /* Hexchat needs to see this event, so 
								don't eat it. */
}

static int autooptoggle_cb(char *word[], char *word_eol, void *userdata) {
	enable = !enable;
	hexchat_print(ph, enable ? "AutoOping now enabled!\n" : "AutoOping now Disalbed!\n");
	return HEXCHAT_EAT_ALL; /* eat this command so that Hexchat and
								other plugins can't process it */
}

void hexchat_plugin_get_info(char **name, char ** desc, char **version, void **reserved) {
	*name = PNAME;
	*desc = PDESC;
	*version = PVERSION;
	/* version info */
}

int hexchat_plugin_init(hexchat_plugin *plugin_handle, \
	char **plugin_name, char **plugin_desc, char **plugin_version, \
	char *arg) {
	/* This is where we set the plugin instance to something */
	ph = plugin_handle;
	
	/* give Hexchat the plugin's info */
	*plugin_name = PNAME;
	*plugin_desc = PDESC;
	*plugin_version = PVERSION;
	
	hexchat_hook_command(ph, "AutoOpToggle", HEXCHAT_PRI_NORM, autooptoggle_cb, \
		"Usage: AUTOOPTOGGLE, Turns OFF/ON Auto Oping", 0);
	hexchat_hook_print(ph, "Join", HEXCHAT_PRI_NORM, join_cb, 0);
	hexchat_print(ph, "AutoOpPlugin loaded successfully!\n");
	
	return 1;
}
