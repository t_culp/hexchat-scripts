#include "hexchat-plugin.h"
#include <stdio.h>
#include <stdlib.h>

/* plugin info  used in hexchat_plugin_get_info*/
#define PNAME "Welcome"
#define PDESC "Welcomes anyone who joins"
#define PVERSION "0.1"

static hexchat_plugin *ph;

static int welcome_cb(char *word[], void *userdata) {
	/* strcmp returns 0 if the strings match. */
	if (strcmp(hexchat_get_info(ph, "channel"), "#myveryownchannel") == 0) {
		char *str = (char *)(malloc(sizeof(word[1]) + sizeof("Hey there, ! Welcome to #myveryownchannel.")));
		sprintf(str, "%s%s%s", "Hey there, ", word[1], "! Welcome to #myveryownchannel.");
		hexchat_commandf(ph, "MSG %s %s", word[1], str);
		free(str);
		return HEXCHAT_EAT_NONE;
	}
	return HEXCHAT_EAT_ALL;
}

void hexchat_plugin_get_info(char **name, char ** desc, char **version, void **reserved) {
	*name = PNAME;
	*desc = PDESC;
	*version = PVERSION;
	/* plugin info */
}

int hexchat_plugin_init(hexchat_plugin *plugin_handle, \
	char **plugin_name, char **plugin_desc, char **plugin_version, \
	char *arg) {
	/* This is where we set the plugin instance to something */
	ph = plugin_handle;
	
	/* give Hexchat the plugin's info */
	*plugin_name = PNAME;
	*plugin_desc = PDESC;
	*plugin_version = PVERSION;
	hexchat_hook_print(ph, "Join", HEXCHAT_PRI_NORM, welcome_cb, 0);
	hexchat_print(ph, "Welcome plugin successfully loaded!\n");
	return 1;
}
