#include "hexchat-plugin.h"
#include <stdio.h>
#include <stdlib.h>

#define PNAME "Greeter"
#define PDESC "Sends a message in the channel upon a new arrival."
#define PVERSION "0.1"

static hexchat_plugin *ph;

static int greet_cb(char *word[], void *userdata) {
	/* if the channel is not #myveryownchannel...") */
	if (strcmp(hexchat_get_info(ph, "channel"), "#myveryownchannel") != 0)
		return HEXCHAT_EAT_ALL;
	/* Below is the method for sending a message to the channel via a command */
	hexchat_commandf(ph, "SAY %s%s%s", "Please welcome ", word[1], " to the channel!!!");
	return HEXCHAT_EAT_NONE;
}

static int goodbye_cb(char *word[], void *userdata) {
	if (strcmp(hexchat_get_info(ph, "channel"), "#myveryownchannel") != 0)
		return HEXCHAT_EAT_ALL;
	hexchat_commandf(ph, "SAY %s%s%s", "Who really needed ", word[1], " anyway?");
	return HEXCHAT_EAT_NONE;
}

void hexchat_plugin_get_info(char **name, char ** desc, char **version, void **reserved) {
	*name = PNAME;
	*desc = PDESC;
	*version = PVERSION;
	/* plugin info */
}

int hexchat_plugin_init(hexchat_plugin *plugin_handle, \
	char **plugin_name, char **plugin_desc, char **plugin_version, \
	char *arg) {
	ph = plugin_handle;
	*plugin_name = PNAME;
	*plugin_desc = PDESC;
	*plugin_version = PVERSION;
	hexchat_hook_print(ph, "Join", HEXCHAT_PRI_NORM, greet_cb, 0);
	hexchat_hook_print(ph, "Part", HEXCHAT_PRI_NORM, goodbye_cb, 0);
	hexchat_print(ph, "Greeter plugin successfully loaded.");
	return 1;
}

int hexchat_plugin_deinit(hexchat_plugin *plugin_handle, char **plugin_name, char **plugin_desc, char **plugin_version, char *arg) {
	hexchat_print(ph, "Greeter plugin successfully unloaded.");
	return 1;
}

